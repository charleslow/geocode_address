# geocode_address

Use SLA's API to geocode addresses.

# Instructions

Run the following code to geocode an address:

```
addresses = c("110 Holland Avenue", "20 Ghim Moh Road")
geocode_address(addresses)
```